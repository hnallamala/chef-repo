#
# Cookbook Name:: apache
# Recipe:: default
#
# Copyright 2017, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

if node["platform"] == "ubuntu"
		execute "apt-get update -y"
	end


package "apache2" do
	package_name node["apache"]["package"]
	action :install
end

node["apache"]["sites"].each do |sitename,data|
		document_root = "/content/sites/#{sitename}" 
		directory document_root do
			mode "0755"
			recursive true
	end

	if node["platform"] == "ubuntu"
		template_location = "/etc/apache2/sites-enabled/#{sitename}.conf"
	elsif node["platform"] == "centos"
		template_location = "/etc/httpd/conf.d/#{sitename}.conf"
	end

	template template_location do
		source "vhost.erb"
		mode "0644"
		variables(
			:document_root => document_root,
			:port => data["port"],
			:domain => data["domain"]
			)
			notifies :restart, "service[httpd]"
	end
	template "/content/sites/#{sitename}/index.html" do
		source "index.html.erb"
		mode "0644"
		variables(
                        :site_title => data["site_title"]
                        )
	end
end

	if node["platform"] == "ubuntu"
		remove_location = "rm -rf /etc/apache2/conf.d/welcome.conf"
		remove_oolcaton = "rm -rf /etc/apache2/conf.d/README"
	elsif node["platform"] == "centos"
		remove_location = "rm -rf /etc/httpd/conf.d/welcome.conf"
		remove_oolcaton = "rm -rf /etc/httpd/conf.d/README"
	end

	execute remove_location do
		only_if do
			File.exist?("/etc/apache2/conf.d/welcome.conf")
		end
		notifies :restart, "service[httpd]"
	end

	execute remove_oolcaton do
        	only_if do
                	File.exist?("/etc/apache2/conf.d/README")
        	end
        	notifies :restart, "service[httpd]"
	end
							
service "httpd" do
	service_name node["apache"]["package"]
	action [:enable,:start]
end

include_recipe "php::default"
