default["apache"]["sites"]["nallamalahk2"] = {"site_title" => "Nallamala HK 2" ,"port" => 80, "domain" => "nallamalahk2.mylabserver.com "}
default["apache"]["sites"]["nallamalahk2b"] = {"site_title" => "Nallamala HK 2b","port" => 80, "domain" => "nallamalahk2b.mylabserver.com "}
default["apache"]["sites"]["nallamalahk3"] = {"site_title" => "Nallamala HK 3" ,"port" => 80, "domain" => "nallamalahk3.mylabserver.com "}
default["apache"]["sites"]["nallamalahk3b"] = {"site_title" => "Nallamala HK 3b" ,"port" => 80, "domain" => "nallamalahk3b.mylabserver.com "}

case node["platform"]
when "centos"
	default["apache"]["package"] = "httpd"
	default["php"]["package"] = "php"
when "ubuntu" 
	default["apache"]["package"] = "apache2"
	default["php"]["package"] = "php5"
end


